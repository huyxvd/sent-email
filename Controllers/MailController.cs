using System.Net;
using System.Net.Mail;
using Microsoft.AspNetCore.Mvc;

namespace mail_demo.Controllers;

[ApiController]
[Route("[controller]")]
public class MailController : ControllerBase
{
    public MailController()
    {
    }
    //     "MailConfigurations": {
    //     "DisplayName": "SMTP",
    //     "From": "smtpmail099@gmail.com",
    //     "Host": "smtp.gmail.com",
    //     "Password": "maxkuivvanlczugy",
    //     "Port": 587,
    //     "UserName": "smtpmail099@gmail.com",
    //     "UseSSL": true,
    //     "UseStartTls": true
    //   }

    [HttpGet] // /Mail?recipientEmail=toSomeOne@gmail.com&subject=subject&content=hello
    public async Task GetAsync(string recipientEmail, string subject, string content)
    {
        var fromAddress = new MailAddress("smtpmail099@gmail.com");
        var toAddress = new MailAddress(recipientEmail);

        var smtpClient = new SmtpClient("smtp.gmail.com", 587)
        {
            UseDefaultCredentials = false,
            Credentials = new NetworkCredential("smtpmail099@gmail.com", "maxkuivvanlczugy"),
            EnableSsl = true
        };

        var message = new MailMessage(fromAddress, toAddress)
        {
            Subject = subject,
            Body = content
        };

        await smtpClient.SendMailAsync(message);
    }
}
